//
//  Webviewmenus.swift
//  PlazaMalta
//
//  Created by elaniin on 12/18/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import Foundation
import WebKit
import NVActivityIndicatorView
class Webviewmenus: UIViewController,WKNavigationDelegate, NVActivityIndicatorViewable{

        @IBOutlet weak var webview: WKWebView!
        var getLink = String()
        var carga = Bool()
    
    override func viewDidLoad() {
        webview.navigationDelegate = self
        carga = false
        
        self.startAnimating()
        let website = self.getLink
        
        
        let urlStr : NSString = website.addingPercentEscapes(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSString
        let searchURL : NSURL = NSURL(string: urlStr as String)!
        let request = NSURLRequest(url: searchURL as URL)
        webview.load(request as URLRequest)
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.stopAnimating()
    }
    
    @IBAction func goBack(_ sender: AnyObject) {
        
        DispatchQueue.main.async(execute: {
            self.dismiss(animated: true) {
                print("finished")
            }
        })
    }
    

}
