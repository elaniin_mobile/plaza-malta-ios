//
//  Gardient2.swift
//  AppCelulas
//
//  Created by elaniin on 12/6/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import Foundation

//
//  Gradient.swift
//  AppCelulas
//
//  Created by elaniin on 11/16/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import Foundation
@IBDesignable class Gradient2 : UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    
    @IBInspectable var borderwidth: CGFloat = 0{
        didSet{
            self.layer.borderWidth = 0.0
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        let gradientColor1 =     GeneralAttributes.hexStringToUIColor(hex: "#2DBEBB").cgColor
        let gradientColor2 =  GeneralAttributes.hexStringToUIColor(hex: "#1f8582").cgColor
        
        
        self.frame.origin = CGPoint(x: (((superview?.frame.width)! / 2) - (self.frame.width / 2)), y: self.frame.origin.y)
        
        
        self.clipsToBounds = true
        
        
        let btnGradient = CAGradientLayer()
        btnGradient.frame = self.bounds
        btnGradient.colors = [gradientColor1, gradientColor2]
        btnGradient.startPoint = CGPoint(x: 1, y: 0.5)
        btnGradient.endPoint = CGPoint(x: 0, y: 0.5)
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        CATransaction.commit()
        self.layer.addSublayer(btnGradient)
        
        
    }
    
    
    
}
