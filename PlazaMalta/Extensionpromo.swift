//
//  Extensionpromo.swift
//  PlazaMalta
//
//  Created by elaniin on 12/1/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import TBEmptyDataSet

extension promos: TBEmptyDataSetDataSource, TBEmptyDataSetDelegate {
    // MARK: - TBEmptyDataSet data source
    /*
    func imageForEmptyDataSet(in scrollView: UIScrollView) -> UIImage? {
        return #imageLiteral(resourceName: "Group 9522")
    }*/
    
    func titleForEmptyDataSet(in scrollView: UIScrollView) -> NSAttributedString? {
        let title = "No hay promos disponibles"
        var attributes: [NSAttributedStringKey: Any]?
        attributes = [NSFontAttributeName as NSString: UIFont(name: "Lato", size: 19)!]
        
        return NSAttributedString(string: title, attributes: attributes as! [String : Any])
    }
    /*
    func descriptionForEmptyDataSet(in scrollView: UIScrollView) -> NSAttributedString? {
        let description = "Revise su conexión a Internet"
        var attributes: [NSAttributedStringKey: Any]?
        attributes = [NSFontAttributeName as NSString: UIFont(name: "Lato", size: 19)!]
        
        return NSAttributedString(string: description, attributes: attributes as! [String : Any])
    }*/
    
    func verticalOffsetForEmptyDataSet(in scrollView: UIScrollView) -> CGFloat {
        if let navigationBar = navigationController?.navigationBar {
            return -navigationBar.frame.height * 0.80
        }
        return 0
    }
    
    func verticalSpacesForEmptyDataSet(in scrollView: UIScrollView) -> [CGFloat] {
        return [25, 8]
    }
    
 
    
    // MARK: - TBEmptyDataSet delegate
    func emptyDataSetShouldDisplay(in scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func emptyDataSetTapEnabled(in scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func emptyDataSetScrollEnabled(in scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func emptyDataSetDidTapEmptyView(in scrollView: UIScrollView) {
        
    }
    
    func emptyDataSetWillAppear(in scrollView: UIScrollView) {
        print("EmptyDataSet Will Appear!")
    }
    
    func emptyDataSetDidAppear(in scrollView: UIScrollView) {
        print("EmptyDataSet Did Appear!")
    }
    
    func emptyDataSetWillDisappear(in scrollView: UIScrollView) {
        print("EmptyDataSet Will Disappear!")
    }
    
    func emptyDataSetDidDisappear(in scrollView: UIScrollView) {
        print("EmptyDataSet Did Disappear!")
    }
}
