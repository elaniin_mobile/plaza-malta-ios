//
//  SeccionCollectionReusableView.swift
//  PlazaMalta
//
//  Created by alex on 9/18/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import UIKit

class SeccionCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var image: UIImageView!
}
