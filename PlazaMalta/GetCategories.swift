//
//  GetCategories.swift
//  PlazaMalta
//
//  Created by alex on 10/26/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import Foundation
struct GetCategories { //Make some suitable name
    let id: String
    let name: String
    let tipo: String
    let website: String
    let email: String
    let telefono: String
    let telefono2: String
    let facebook: String
    let twitter: String
    let instagram: String
    let area: String
    let horarios: NSDictionary
    let logo: String
    let descrip: String
    let menu: String
    
    init(dictionary: [String:Any]) {
        self.id = dictionary["id"] as? String ?? "Default Id"
        self.name = dictionary["titulo"] as? String ?? "Default title"
        self.tipo = dictionary["tipo"] as? String ?? "Default address"
        self.website = dictionary["website"] as? String ?? "Default website"
        self.email = dictionary["email"] as? String ?? "Default email"
        self.telefono = dictionary["telefono"] as? String ?? "Default telefono"
        self.telefono2 = dictionary["telefono2"] as? String ?? "Default telefono"
        self.facebook = dictionary["facebook"] as? String ?? "Default facebook"
        self.twitter = dictionary["twitter"] as? String ?? "Default twitter"
        self.instagram = dictionary["instagram"] as? String ?? "Default district"
        self.area = dictionary["instagram"] as? String ?? "Default instagram"
        self.horarios = (dictionary["horarios"] as? NSDictionary)!
        self.logo = dictionary["logo"] as? String ?? "Default rating"
        self.descrip = dictionary["descripcion"] as? String ?? "Default descripcion"
        self.menu = (dictionary["menu"] as? String)!
        
    }
}
