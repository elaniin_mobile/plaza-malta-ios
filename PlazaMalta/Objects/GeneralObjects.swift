//
//  GeneralObjects.swift
//  PlazaMalta
//
//  Created by elaniin on 12/19/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import Foundation

struct callUrl{
    var baseURL = "http://plazamalta.com.sv/"
    var getshopCategories = "http://plazamalta.com.sv/wp-json/v1/categorias-tiendas"
    var getShopinfo = "http://plazamalta.com.sv/wp-json/v1/tiendas/"
    var getRestaurantsinfo = "http://plazamalta.com.sv/wp-json/v1/restaurantes/"
    var getRestaurantcategories = "http://plazamalta.com.sv/wp-json/v1/categorias-restaurantes"
    var generalShopsinfo  = "http://plazamalta.com.sv/wp-json/v1/all"
    var getEvents = ""
    var getPromos = ""

}

struct errorMessages {
    let titleError = "Error"
    let errorName = "Debe ingresar un nombre"
    let emaiFieldlEmpty = "Debe ingresar un correo"
    let invalidEmail = "Debe ingresar un correo valido"
    let passwordFieldEmpty = "Por favor complete todos los campos"
    let confirmFieldEmpty = "El campo de validación de contraseña no puede quedar vacio"
    let incompatiblePasswords = "Las contraseñas no coinciden"
    let lengthPasswordError = "La contraseña debe contener al menos 8 caracteres"
    let siginEmailError = "Sucedio algo con su registro, por favor verifique todos los campos"
    let passwordSiginInFieldEmpty = "Debe ingresar una contraseña"
    let invalidSignInData = "Sus datos son invalidos. Intente de nuevo"

}


