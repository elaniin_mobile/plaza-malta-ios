//
//  TableviewHelper.swift
//  PlazaMalta
//
//  Created by elaniin on 10/6/17.
//  Copyright © 2017 elaniin. All rights reserved.
//
import Foundation
import UIKit

class TableViewHelper {
    
    class func EmptyMessage(message:String, viewController:UITableViewController) {
        let messageLabel = UILabel()
        messageLabel.text = message
        messageLabel.textColor = UIColor.white
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "Lato", size: 15)
        messageLabel.sizeToFit()
        
        viewController.tableView.backgroundView = messageLabel;
        viewController.tableView.separatorStyle = .none;
    }
    
    class func EmptyMessage2(message:String, viewController:UIViewController, table:UITableView) {
        
        let messageLabel = UILabel()
        messageLabel.text = message
        messageLabel.textColor = .white
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "Lato", size: 15)
        messageLabel.sizeToFit()
        
        table.backgroundView = messageLabel;
        table.separatorStyle = .none;
    }
 
}
