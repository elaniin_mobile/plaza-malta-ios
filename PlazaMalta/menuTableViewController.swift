//
//  menuTableViewController.swift
//  EconoParts
//
//  Created by Adrian Gomez on 12/18/16.
//  Copyright © 2016 elaniin. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FBSDKLoginKit
import GoogleSignIn
import Fabric
import Crashlytics


class menuTableViewController: UITableViewController {
    struct defaultsKeys {
        static let keyOne = "firstStringKey"
        static let keyTwo = "secodStringKey"
    }
    let defaults = UserDefaults.standard
    @IBOutlet var table: UITableView!
    var user_id:String = ""
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var aView: UIView!
    var url:URL! = nil
    
    let storage = Storage.storage().reference()
    let ref = Database.database().reference()
     let prevUser = Auth.auth().currentUser
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        general()
        setupProfile()
        facebookdata()
        //googledata()
    
    }
    
    func general(){
    GeneralAttributes.circleimage(imageview: self.avatar)
        var y = String()
        if let stringOne = defaults.string(forKey: menuTableViewController.defaultsKeys.keyTwo) {
            print("aaa"+stringOne) // Some String Value
            y = stringOne
        }
    if Auth.auth().currentUser?.uid == nil && y == ""{
    FBSDKAccessToken.setCurrent(nil)
    FBSDKProfile.setCurrent(nil)
    self.avatar?.image = UIImage(named: "profile")
        self.name.text = "Invitado"
        logout()
    
    }
        
    
    
      
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }
    
    
    func facebookdata(){
        guard let accessToken = FBSDKAccessToken.current() else {
            print("Failed to get access token")
            return
        }
        do {
            if Auth.auth().currentUser?.uid == nil{
                FBSDKAccessToken.setCurrent(nil)
                FBSDKProfile.setCurrent(nil)
                logout()
            }else{
                let credential = try! FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                
                defaults.set(FBSDKAccessToken.current().tokenString, forKey: defaultsKeys.keyOne)
                if let stringOne = defaults.string(forKey: defaultsKeys.keyOne) {
                    print("aaa"+stringOne) // Some String Value
                }
                Auth.auth().signIn(with: credential) { (user, error) in
                    if let error = error {
                        print("Facebook login failed. Error \(error)")
                        return
                    }
                    
                    self.name.text = user?.displayName
                    
                    let url = URL(string: (user?.photoURL?.absoluteString)!)
                    DispatchQueue.global().async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            self.avatar.image = UIImage(data: data!)
                        }
                    }
                }
            }
            
        }catch{
            print(error.localizedDescription)
            
        }
        
    }

    func googledata(){
        if let user = Auth.auth().currentUser {
            name.text = user.displayName
            
            let url = URL(string: (user.photoURL?.absoluteString)!)
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!)
                DispatchQueue.main.async {
                    self.avatar.image = UIImage(data: data!)
                }
            }
        }

    }


    func logout(){
         GIDSignIn.sharedInstance().signOut()
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
        
        try! Auth.auth().signOut()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        FBSDKAccessToken.setCurrent(nil)
        FBSDKProfile.setCurrent(nil)
        let loginViewController = storyboard.instantiateViewController(withIdentifier: "loginscreen")
        present(loginViewController, animated: true, completion: nil)
    }
    
    @IBAction func logout(_ sender: Any) {
        logout()
    }

    
    @IBAction func instagram(_ sender: Any) {
        let instagram = "https://www.instagram.com/plazamalta/"
        if(instagram != ""){
            var instaUrl = NSURL(string: instagram)
            if UIApplication.shared.canOpenURL(instaUrl! as URL)
            {
                UIApplication.shared.openURL(instaUrl! as URL)
                
            } else {
                //redirect to safari because the user doesn't have Instagram
                UIApplication.shared.openURL(NSURL(string: instagram)! as URL)
            }
        }else{
           
        }
        
        
    }
    
    @IBAction func showfb(_ sender: Any) {
        let instagram = "https://www.facebook.com/PlazaMalta/?fref=ts"
        if(instagram != ""){
            var instaUrl = NSURL(string: instagram)
            if UIApplication.shared.canOpenURL(instaUrl! as URL)
            {
                UIApplication.shared.openURL(instaUrl! as URL)
                
            } else {
                //redirect to safari because the user doesn't have Instagram
                UIApplication.shared.openURL(NSURL(string: instagram)! as URL)
            }
        }else{
            
        }
        
        
    }
    
    
    @IBOutlet weak var facebook: UIButton!
    
    func setupProfile(){
        
        
        avatar.layer.cornerRadius = avatar.frame.size.width/2
        avatar.clipsToBounds = true
        if let uid = Auth.auth().currentUser?.uid{
            
                 ref.child("users").child(uid).observe(.value, with: { (snapshot) in
             
               
                if let dict = snapshot.value as? [String: AnyObject]
                {
                    
                    self.name.text = dict["username"] as? String
                    
                    if let profileImageURL = dict["photo"] as? String
                    {
                        
                        self.url = URL(string: profileImageURL)
                        
                        
                        URLSession.shared.dataTask(with: self.url!, completionHandler: { (data, response, error) in
                            if error != nil{
                                print(error!)
                                return
                            }
                            DispatchQueue.main.async {
                                if(self.url != nil){
                                    self.avatar?.image = UIImage(data: data!)
                                }else{
                                    self.avatar?.image = UIImage(named: "profile")
                                }
                                
                            }
                        }).resume()
                    }
                    }
                 })
            
        }
    }



}





