//
//  MapViewController.swift
//  PlazaMalta
//
//  Created by alex on 8/29/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var op: UIBarButtonItem!
    let location = CLLocationCoordinate2D(latitude: 13.6671443, longitude: -89.2709431)
    
    // Set MapKit
    let span = MKCoordinateSpanMake(0.05, 0.05)
    
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
     
        map.mapType = MKMapType.standard
        map.showsTraffic = true
        
        // 2)
        let region =  MKCoordinateRegionMakeWithDistance(location, 165, 160);
        
        map.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = "Plaza Malta"
        annotation.subtitle = "Santa Elena"
    
        map.addAnnotation(annotation)
        
        let searchImage = UIImage(named:"logoh.png")?.withRenderingMode(.alwaysOriginal)
        
        
        
        let searchButton = UIBarButtonItem(image: searchImage,  style: .plain, target: self, action: nil)
        

        navigationItem.leftBarButtonItems = [self.op, searchButton]

        
        //Main
        if self.revealViewController() != nil {
            op.target = self.revealViewController()
            op.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        let color1 = self.hexStringToUIColor(hex: "#25333F")
        let color2 = self.hexStringToUIColor(hex: "#2DBEBB")
        
       
        
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackOpaque
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = color1;
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: color2]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as! [String : Any]

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func openMapForPlace() {
        
        let latitude: CLLocationDegrees = 13.6671443
        let longitude: CLLocationDegrees = -89.2709431
        
        let regionDistance:CLLocationDistance = 150
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "Plaza Malta"
        mapItem.openInMaps(launchOptions: options)
         /*UIApplication.shared.openURL(URL(string:"https://www.google.com/maps/@13.6671443,-89.27094316z")!)*/
    }
    
        @IBAction func Maps(_ sender: Any) {
            openMapForPlace()
        }
    
    
    @IBAction func waze(_ sender: Any) {
        let latitude: CLLocationDegrees = 13.6671443
        let longitude: CLLocationDegrees = -89.2709431
        
        var link:String = "waze://"
        var url:NSURL = NSURL(string: link)!
        
        if UIApplication.shared.canOpenURL(url as URL) {
            
            var urlStr:NSString = NSString(format: "waze://?ll=%f,%f&navigate=yes",latitude, longitude)
            
            UIApplication.shared.openURL(NSURL(string: urlStr as String)! as URL)
            UIApplication.shared.isIdleTimerDisabled = true
            
            
        }
        else {
            //redirect to safari because the user doesn't have Instagram
            UIApplication.shared.openURL(NSURL(string: "https://www.waze.com/es-419/livemap?zoom=17&lat=13.66723&lon=-89.27059")! as URL)
        }
    }
    
    
    
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    

    


}
